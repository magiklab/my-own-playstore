package vn.magik.moreapps.callLib;

import android.content.Context;

import com.google.gson.Gson;

import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import vn.magik.moreapps.model.Category;
import vn.magik.moreapps.retrofitapi.UserServices;
import vn.magik.moreapps.utils.HandleDataLocal;
import vn.magik.moreapps.model.DataResponse;
import vn.magik.moreapps.utils.Utils;

/**
 * Created by thaitien on 10/14/2016.
 */

public class InitLib {
    private static InitLib instance = null;
    private HandleDataLocal mHandleDataLocal;
    private CallBackHandleView callBackHandleView;
    private static final int LIB_VERSION = 3;
    private boolean isInitLib = false;

    public static InitLib getInstance() {
        if (instance == null) {
            instance = new InitLib();
        }
        return instance;
    }

    public void subscribeUpdateView(CallBackHandleView callBackHandleView) {
        this.callBackHandleView = callBackHandleView;
    }


    public InitLib initLab(Context mContext, String urlServer) {
        if (!isInitLib) {
            isInitLib = true;
            mHandleDataLocal = new HandleDataLocal(mContext);
            String languageCode = Locale.getDefault().getLanguage();
            String countryCode = Locale.getDefault().getCountry();
            String packageName = mContext.getApplicationContext().getPackageName();
            String conj = "?";
            if (urlServer.contains("?")) conj = "&";
            urlServer = String.format("%s%slanguage=%s&country=%s&android_package=%s&version=%s",
                    urlServer, conj, languageCode, countryCode, packageName, LIB_VERSION);

            if (Utils.isOnline(mContext)) {
                loadDataOnline(urlServer);
            } else {
                String jsonString = mHandleDataLocal.getData();
                if (jsonString != null) {
                    if (callBackHandleView != null) {
                        callBackHandleView.onFinishLoadServer(0);
                    }
                }
            }
        }
        return instance;
    }

    private void loadDataOnline(String urlServer) {
        UserServices userServices = new UserServices();
        String language = Locale.getDefault().getLanguage();
        userServices.getListData(urlServer, language, new Callback<DataResponse>() {
            @Override
            public void onResponse(Call<DataResponse> call, Response<DataResponse> response) {
                if (response != null && response.isSuccessful() && response.body() != null) {
                    DataResponse dataResponse = response.body();
                    List<Category> categories = dataResponse.getData().getCategories();
                    for (int i = categories.size() - 1; i >= 0; i--) {
                        if (categories.get(i).getApps().size() == 0)
                            categories.remove(i);
                    }
                    int revision = getRevisionChanged(mHandleDataLocal.getData());
                    String jsonString = new Gson().toJson(response.body());
                    mHandleDataLocal.saveData(jsonString);
                    if (callBackHandleView != null) {
                        callBackHandleView.onFinishLoadServer(dataResponse.getData().getRevision() - revision);
                    }
                }
            }

            @Override
            public void onFailure(Call<DataResponse> call, Throwable t) {
                t.printStackTrace();
            }
        });
    }

    private int getRevisionChanged(String jsonString) {
        if (jsonString != null) {
            return new Gson().fromJson(jsonString, DataResponse.class).getData().getRevision();
        } else return 0;
    }

}
