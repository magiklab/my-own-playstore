package vn.magik.moreapps.fragment;

import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.gson.Gson;

import java.util.List;

import vn.magik.moreapps.R;
import vn.magik.moreapps.adapter.CategoryAdapter;
import vn.magik.moreapps.callLib.CallBackHandleView;
import vn.magik.moreapps.callLib.InitLib;
import vn.magik.moreapps.model.Category;
import vn.magik.moreapps.model.DataResponse;
import vn.magik.moreapps.utils.HandleDataLocal;


public class AppsFragment extends Fragment {
    private ListView listView;
    private HandleDataLocal mHandleDataLocal;
    private boolean isUpdateViewFromCallback = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main, container, false);
        listView = (ListView) view.findViewById(R.id.listView);
        mHandleDataLocal = new HandleDataLocal(getActivity());
        onUpdateView();
        return view;
    }

    private void onUpdateView() {
        if (listView != null) {
            String categoriesJson = mHandleDataLocal.getData();
            if (categoriesJson != null) {
                List<Category> categories = new Gson()
                        .fromJson(categoriesJson, DataResponse.class)
                        .getData()
                        .getCategories();
                CategoryAdapter listAdapter = new CategoryAdapter(getActivity(), categories);
                listView.setAdapter(listAdapter);
            } else {
                if (!isUpdateViewFromCallback) {
                    InitLib.getInstance().subscribeUpdateView(new CallBackHandleView() {
                        @Override
                        public void onFinishLoadServer(int totalApps) {
                            isUpdateViewFromCallback = true;
                            onUpdateView();
                        }
                    });
                }
            }
        }
    }

}
