package vn.magik.moreapps.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import java.util.List;

import vn.magik.moreapps.R;
import vn.magik.moreapps.model.App;
import vn.magik.moreapps.model.Category;
import vn.magik.moreapps.type.TypeRender;
import vn.magik.moreapps.utils.LoadImage;
import vn.magik.moreapps.utils.Utils;

/**
 * Created by ThaiVanTien on 9/30/2016.
 */

public class CategoryAdapter extends BaseAdapter {
    private Context mContext;
    private List<Category> mDataSet;
    private int sizeCardView;


    public CategoryAdapter(Context mContext, List<Category> mDataSet) {
        this.mContext = mContext;
        this.mDataSet = mDataSet;
        sizeCardView = Utils.dipToPixel(mContext, 3);
    }

    @Override
    public int getCount() {
        return mDataSet.size();
    }

    @Override
    public Object getItem(int i) {
        return mDataSet.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    private class ViewHolder {
        TextView tvDescription, tvTitle;
        RecyclerView rvContainApp;
        ViewFlipper vpViewFeature;
        CardView cvCardOfList;
    }

    @Override
    public View getView(int positon, View convertView, ViewGroup viewGroup) {
        ViewHolder holder = null;
        if (convertView == null) {
            convertView = LayoutInflater.from(mContext).inflate(R.layout.view_category, null);
            holder = new ViewHolder();
            holder.tvTitle = (TextView) convertView.findViewById(R.id.tvTitile);
            holder.tvDescription = (TextView) convertView.findViewById(R.id.tvDescription);
            holder.cvCardOfList = (CardView) convertView.findViewById(R.id.cv_card_of_list);
            holder.rvContainApp = (RecyclerView) convertView.findViewById(R.id.rv_view_grid);
            holder.vpViewFeature = (ViewFlipper) convertView.findViewById(R.id.vp_view_feature);
            Animation in = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_in_left);
            Animation out = AnimationUtils.loadAnimation(mContext, android.R.anim.slide_out_right);
            holder.vpViewFeature.setInAnimation(in);
            holder.vpViewFeature.setOutAnimation(out);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        Category category = mDataSet.get(positon);
        //set title and description
        if (category.getName() != null && !category.getName().equals("")) {
            holder.tvTitle.setText(category.getName());
            holder.tvTitle.setVisibility(View.VISIBLE);
        } else {
            holder.tvTitle.setVisibility(View.GONE);
        }
        if (category.getDescription() != null && !category.getDescription().equals("")) {
            holder.tvDescription.setText(Html.fromHtml(category.getDescription()));
            holder.tvDescription.setVisibility(View.VISIBLE);
        } else {
            holder.tvDescription.setVisibility(View.GONE);
        }
        //render contain category type
        if (category.getType().equals(TypeRender.FEATURE)) {
            showAppsFeature(holder, category.getApps());
        } else {
            holder.vpViewFeature.setVisibility(View.GONE);
            holder.rvContainApp.setVisibility(View.VISIBLE);
            if (category.getType().equals(TypeRender.GRID)) {
                holder.cvCardOfList.setCardElevation(0);
                holder.rvContainApp.setLayoutManager(new LinearLayoutManager(mContext.getApplicationContext(), LinearLayoutManager.HORIZONTAL, false));
                holder.rvContainApp.setNestedScrollingEnabled(false);
                holder.rvContainApp.setAdapter(new ListAppAdapter(mContext, mDataSet.get(positon).getApps(), R.layout.view_app_gird));
            } else {
                holder.cvCardOfList.setCardElevation(sizeCardView);
                holder.rvContainApp.setLayoutManager(new LinearLayoutManager(mContext.getApplicationContext(), LinearLayoutManager.VERTICAL, false));
                holder.rvContainApp.setNestedScrollingEnabled(false);
                holder.rvContainApp.setAdapter(new ListAppAdapter(mContext, mDataSet.get(positon).getApps(), R.layout.view_app_list));
            }
        }
        return convertView;
    }

    private void showAppsFeature(ViewHolder viewHolder, final List<App> apps) {
        viewHolder.vpViewFeature.setVisibility(View.VISIBLE);
        viewHolder.rvContainApp.setVisibility(View.GONE);
        for (int i = 0; i < apps.size(); i++) {
            final ImageView imageView = new ImageView(mContext);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
            LoadImage.getImage(mContext, apps.get(i).getFeatureImage(), imageView);
            viewHolder.vpViewFeature.addView(imageView);
            imageView.setTag(apps.get(i).getActionLink());
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (imageView.getTag() != null) {
                        String actionLink = (String) imageView.getTag();
                        mContext.startActivity(new Intent(Intent.ACTION_VIEW,
                                Uri.parse(actionLink)));
                    }
                }
            });
        }
        if (apps.size() > 1) {
            viewHolder.vpViewFeature.setFlipInterval(5000);
            viewHolder.vpViewFeature.setAutoStart(true);
        }
    }
}
