package vn.magik.moreapps.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class App implements Serializable {
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    @SerializedName("feature_image")
    private String featureImage;
    @SerializedName("icon_url")
    private String icon_URL;
    @SerializedName("android")
    private String android;
    @SerializedName("web")
    private String web;
    @SerializedName("description")
    private String description;
    @SerializedName("action_link")
    private String actionLink;

    public App(int id, String name,String featureImage,
               String icon_URL, String android, String web, String description, String actionLink) {
        this.id = id;
        this.name = name;
        this.featureImage = featureImage;
        this.icon_URL = icon_URL;
        this.android = android;
        this.web = web;
        this.description = description;
        this.actionLink = actionLink;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }


    public String getFeatureImage() {
        return featureImage;
    }


    public String getIcon_URL() {
        return icon_URL;
    }

    public String getAndroid() {
        return android;
    }
    public String getActionLink() {
        return actionLink;
    }

    public String getWeb() {
        return web;
    }

    public String getDescription() {
        return description;
    }
}
