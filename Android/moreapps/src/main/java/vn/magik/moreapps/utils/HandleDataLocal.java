package vn.magik.moreapps.utils;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by dk-darkness on 04/11/2016.
 */

public class HandleDataLocal {
    private SharedPreferences sharedPreferences;

    public HandleDataLocal(Context mContext) {
        sharedPreferences = mContext.getSharedPreferences(Constant.PREFERENCES_SAVEDATA, Context.MODE_PRIVATE);
    }

    public void saveData(String profile) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(Constant.PREFERENCES_SAVEDATA, profile);
        editor.apply();
    }

    public String getData() {
        return sharedPreferences.getString(Constant.PREFERENCES_SAVEDATA, null);
    }
}
