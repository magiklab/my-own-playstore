package vn.magik.moreapps.activities;

import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.MenuItem;

import vn.magik.moreapps.R;
import vn.magik.moreapps.callLib.InitLib;
import vn.magik.moreapps.utils.Constant;

public class MoreAppsActivity extends AppCompatActivity {
    public String API = "API";
    public String DISPLAY_EXIT = "DISPLAY_EXIT";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_app);
        initView();
    }

    public void initView() {
        String apiConfig = null;
        boolean isDisplayExit = false;
        try {
            ActivityInfo app = getPackageManager().getActivityInfo(this.getComponentName(), PackageManager.GET_META_DATA);
            Bundle bundle = app.metaData;
            apiConfig = bundle.getString(API);
            isDisplayExit = bundle.getBoolean(DISPLAY_EXIT, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (isDisplayExit) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(isDisplayExit);
            }
        }
        if (apiConfig == null || apiConfig.equals("")) {
            apiConfig = Constant.MY_API;
        }
        InitLib.getInstance().initLab(this, apiConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }
}
