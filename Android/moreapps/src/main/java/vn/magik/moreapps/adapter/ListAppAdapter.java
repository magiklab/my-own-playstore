package vn.magik.moreapps.adapter;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import androidx.recyclerview.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import vn.magik.moreapps.R;
import vn.magik.moreapps.model.App;
import vn.magik.moreapps.utils.LoadImage;

/**
 * Created by ThaiVanTien on 9/30/2016.
 */

public class ListAppAdapter extends RecyclerView.Adapter<ListAppAdapter.MyHolderView> {

    private List<App> mDataSet;
    private Context context;
    private int layoutContain;

    public class MyHolderView extends RecyclerView.ViewHolder {
        private TextView txtTitle, txtIsInstall, tvDescription;
        private ImageView imgView;
        private View viewBreak;

        private MyHolderView(View itemView) {
            super(itemView);
            txtTitle = (TextView) itemView.findViewById(R.id.tvTitle);
            imgView = (ImageView) itemView.findViewById(R.id.imgCategory);
            txtIsInstall = (TextView) itemView.findViewById(R.id.txtIsInstall);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            viewBreak = itemView.findViewById(R.id.view_break);
        }
    }


    public ListAppAdapter(Context context, List<App> listDemo, int layoutContain) {
        this.context = context;
        this.mDataSet = listDemo;
        this.layoutContain = layoutContain;
    }

    @Override
    public MyHolderView onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(layoutContain, parent, false);
        return new MyHolderView(view);
    }

    @Override
    public void onBindViewHolder(MyHolderView holder, int position) {
        final App app = mDataSet.get(position);
        holder.txtTitle.setText(app.getName());
        //set app isInstall();
        if (isAppInstall(app.getAndroid())) {
            holder.txtIsInstall.setText("");
            holder.txtIsInstall.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_briefcase_check, 0, 0, 0);
        }
        if (holder.tvDescription != null && app.getDescription() != null) {
            holder.tvDescription.setText(Html.fromHtml(app.getDescription()));
        }
        //update break item
        if (holder.viewBreak != null) {
            if (position == mDataSet.size() - 1) {
                holder.viewBreak.setVisibility(View.GONE);
            } else {
                holder.viewBreak.setVisibility(View.VISIBLE);
            }
        }

        LoadImage.getImage(context, app.getIcon_URL(), holder.imgView);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(Intent.ACTION_VIEW,
                        Uri.parse(app.getActionLink())));
            }
        });
    }

    public boolean isAppInstall(String url) {
        Intent itent = context.getPackageManager().getLaunchIntentForPackage(url);
        if (itent != null) {
            return true;
        }
        return false;
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }


}
