# My-own-playstore
Make a android store for your company, your team.

# Config:
Server:
Create Api return as http://work.magik.vn/api.php

Android Client:
add library to dependencies:

    dependencies {
        compile 'vn.magik.libraries:moreapps:2.0'
    }
    
Add Activity to manifest
    ```
    <activity
        android:name="vn.magik.moreapps.activities.MoreAppsActivity"
        android:label="@string/app_name"
        android:screenOrientation="portrait">
        <intent-filter>
            <action android:name="android.intent.action.MAIN" />
            <category android:name="android.intent.category.LAUNCHER" />
        </intent-filter>
        <meta-data
            android:name="API"      // change api
            android:value="http://appserver.magik.vn/more_app/api.php" />
        <meta-data
            android:name="DISPLAY_EXIT"     //display back button (more app)
            android:value="false" />
    </activity>
    ```
    
We have Fragment "AppsFragment", so we can add Fragment to Activty:

Function Init load api to update view(more app)

    public static final String URL = "http://work.magik.vn/api.php";
    InitLib.getInstance().initLab(MagikAppsActivity.this, URL).
        subscribeUpdateView(new CallBackHandleView() {
            @Override
            public void onFinishLoadServer(int newApp) {
                //update new app as notification.
            }
        });
    
    
Function add fragment to View, you must to call function Init before add Fragment to view

    private void addFragment(){
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        AppsFragment appsFragment = new AppsFragment();
        fragmentTransaction.add(R.id.view_contain, appsFragment);
        fragmentTransaction.commit();
    }
    
Function start activity more app:
    Intent intent = new Intent(mContext, MoreAppsActivity.class);
    startActivity(intent);

# Build Library to Jcenter
```
gradlew clean build generateRelease
```
https://medium.com/@daniellevass/how-to-publish-your-android-studio-library-to-jcenter-5384172c4739

# View sample

Aplication of Magiklab:

![alt tag](https://lh3.googleusercontent.com/kES48RhXSEe1J4W-W2vpU-F_T4qiUIdUOFIhD9tF987bPC3L-MNrX0vy37lxkZnf7rE=h310-rw)
![alt tag](https://lh3.googleusercontent.com/Y99IO5J-ZZmCVIwTun1M9B6cjG9mHh-JUeCIo3l2PB_t5sMB9Ru4IcMUC8EnRTvgEA=h310-rw)

Detail: https://play.google.com/store/apps/details?id=vn.magik.labstore
